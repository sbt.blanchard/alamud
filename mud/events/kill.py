from .event import Event2

class KillEvent(Event2):
    NAME = "kill"

    def perform(self):
        self.inform("kill")
