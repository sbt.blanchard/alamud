from .event import Event3

class FightEvent(Event3):
    NAME = "fight"

    def perform(self):
        self.inform("fight")
