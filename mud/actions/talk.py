from .action import Action2
from mud.events import TalkEvent, TalkOfSmithEvent


class TalkAction(Action2):
    EVENT = TalkEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "talk"


class TalkOfSmithAction(Action2):
    EVENT = TalkOfSmithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "talk-of-smith"
