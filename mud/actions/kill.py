from .action import Action2
from mud.events import KillEvent


class KillAction(Action2):
    EVENT = KillEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "kill"
