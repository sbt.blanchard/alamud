from .action import Action3
from mud.events import FightEvent

class FightAction(Action3):
    EVENT = FightEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "fight"
